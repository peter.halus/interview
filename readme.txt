- Java 11, Spring Boot 2.4.3, Spring Web, Spring Data JPA, Active MQ, H2 Database, Lombok, MapStruct, JUnit, Mockito

- db: embedded h2
    - http://localhost:8080/h2-console
    - connectionstring -> application.properties
- mq: embedded activemq
    - request to queue
        - post http://localhost:8080/api/produce/message
        - Header: "Action" -> "Vloz" || "Vypis" || "ZmazVsetko"