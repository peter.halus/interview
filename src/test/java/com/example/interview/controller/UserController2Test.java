package com.example.interview.controller;

import com.example.interview.InterviewApplication;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import java.net.URI;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {InterviewApplication.class})
public class UserController2Test {

    @Autowired
    private TestRestTemplate template;


    @Test
    public void testCreateUser() throws Exception {
        RequestEntity<String> req = RequestEntity.post(new URI("/api/user"))
                    .contentType(MediaType.APPLICATION_JSON)
                    .body("{ \"userId\": 1, \"userGuid\": \"Skupina 1\", \"userName\": \"Peter\" }");

        ResponseEntity<String> resp = template.postForEntity("/api/user",
                req,
                String.class);
        Assert.assertEquals(HttpStatus.OK, resp.getStatusCode());
    }

    @Test
    public void testGetAllUsers() throws Exception {
        ResponseEntity<String> resp = template.getForEntity("/api/users", String.class);
        Assert.assertEquals(HttpStatus.OK, resp.getStatusCode());
    }

    @Test
    public void testDeleteAllUsers() throws Exception {
        template.delete("/api/users", String.class);
    }
}