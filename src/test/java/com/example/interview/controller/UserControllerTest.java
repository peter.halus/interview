package com.example.interview.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.example.interview.controller.dto.UserDto;
import com.example.interview.service.UserService;
import org.hamcrest.Matchers;
import org.junit.*;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import java.util.Arrays;

@RunWith(SpringJUnit4ClassRunner.class)
public class UserControllerTest {

    private MockMvc mockMvc;

    @InjectMocks
    private UserController userController;

    @Mock
    private UserService userService;

    @Before
    public void setUp() throws Exception {
        mockMvc = MockMvcBuilders.standaloneSetup(userController).build();
    }

    @Test
    public void test_getAllUsers_empty() throws Exception {
        mockMvc.perform(get("/api/users")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", Matchers.hasSize(0)));
    }

    @Test
    public void test_getAllUsers_1() throws Exception {
        UserDto userDto1 = new UserDto();
        userDto1.setUserId(1L);
        userDto1.setUserGuid("group_1");
        userDto1.setUserName("Peter");

        UserDto userDto2 = new UserDto();
        userDto2.setUserId(2L);
        userDto2.setUserGuid("group_1");
        userDto2.setUserName("Martin");

        when(userService.getAllUsers()).thenReturn(Arrays.asList(userDto1, userDto2));

        mockMvc.perform(get("/api/users")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.*", Matchers.hasSize(2)))
                .andExpect(jsonPath("$[*].userId", Matchers.containsInAnyOrder(1, 2)))
                .andExpect(jsonPath("$[*].userGuid", Matchers.containsInAnyOrder("group_1", "group_1")))
                .andExpect(jsonPath("$[*].userName", Matchers.containsInAnyOrder("Peter", "Martin")));
    }

    @Test
    public void test_createUser() throws Exception {
        mockMvc.perform(post("/api/user")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"userId\": 1, \"userGuid\": \"Skupina 1\", \"userName\": \"Peter\" }")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    public void test_deleteAllUsers() throws Exception {
        mockMvc.perform(delete("/api/users")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}