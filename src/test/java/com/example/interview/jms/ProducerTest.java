package com.example.interview.jms;

import com.example.interview.InterviewApplication;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.jms.*;
import java.net.URI;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT, classes = {InterviewApplication.class})
public class ProducerTest {

    private static final String JMS_PATH = "/api/produce/message";

    @Autowired
    private TestRestTemplate template;

    @Test
    @Ignore
    public void test() throws Exception {
        final ConnectionFactory connectionFactory = new ActiveMQConnectionFactory(
                "vm://localhost?broker.persistent=false");

        final Connection connection = connectionFactory.createConnection();
        connection.start();
        final Session session = connection.createSession(false,
                Session.AUTO_ACKNOWLEDGE);
        final Queue queue = session.createTemporaryQueue();
        {
            final MessageProducer producer = session.createProducer(queue);
            final TextMessage message = session.createTextMessage("testing");
            producer.send(message);
        }
        {
            final MessageConsumer consumer = session.createConsumer(queue);
            final TextMessage message = (TextMessage) consumer.receiveNoWait();
            Assert.assertNotNull(message);
            Assert.assertEquals("testing", message.getText());
        }
    }

    // TODO: problem!
    @Test
    public void testSuite() throws Exception {
        // Vloz(1, "a1", "Dusan")
        RequestEntity<String> req = RequestEntity.post(new URI(JMS_PATH))
                .contentType(MediaType.APPLICATION_JSON)
                .header("Action", "Vloz")
                .body("{ \"userId\": 1, \"userGuid\": \"a1\", \"userName\": \"Dusan\" }");

        ResponseEntity<String> resp = template.postForEntity(JMS_PATH,
                req,
                String.class);
        Assert.assertEquals(HttpStatus.OK, resp.getStatusCode());


        // Vloz(2, "a2", "Fero")
        req = RequestEntity.post(new URI(JMS_PATH))
                .contentType(MediaType.APPLICATION_JSON)
                .header("Action", "Vloz")
                .body("{ \"userId\": 2, \"userGuid\": \"a2\", \"userName\": \"Fero\" }");

        resp = template.postForEntity(JMS_PATH,
                req,
                String.class);
        Assert.assertEquals(HttpStatus.OK, resp.getStatusCode());


        // Vypis
        req = RequestEntity.post(new URI(JMS_PATH))
                .contentType(MediaType.APPLICATION_JSON)
                .header("Action", "Vypis")
                .body("");

        resp = template.postForEntity(JMS_PATH,
                req,
                String.class);
        Assert.assertEquals(HttpStatus.OK, resp.getStatusCode());


        // ZmazVsetko
        req = RequestEntity.post(new URI(JMS_PATH))
                .contentType(MediaType.APPLICATION_JSON)
                .header("Action", "ZmazVsetko")
                .body("");

        resp = template.postForEntity(JMS_PATH,
                req,
                String.class);
        Assert.assertEquals(HttpStatus.OK, resp.getStatusCode());


        // Vypis
        req = RequestEntity.post(new URI(JMS_PATH))
                .contentType(MediaType.APPLICATION_JSON)
                .header("Action", "Vypis")
                .body("");

        resp = template.postForEntity(JMS_PATH,
                req,
                String.class);
        Assert.assertEquals(HttpStatus.OK, resp.getStatusCode());
    }
}
