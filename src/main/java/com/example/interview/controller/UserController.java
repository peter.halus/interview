package com.example.interview.controller;

import com.example.interview.controller.dto.UserDto;
import com.example.interview.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;

@RestController
@RequestMapping(
        path = "/api",
        produces = "application/json"
)
public class UserController {

    private static final String USER_PATH = "/user";
    private static final String USERS_PATH = "/users";

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = Objects.requireNonNull(userService,"userService must not be null");
    }


    @GetMapping(USERS_PATH)
    public List<UserDto> getAllUsers() {
        return userService.getAllUsers();
    }

    @PostMapping(USER_PATH)
    public void createUser(@RequestBody UserDto user) {
        userService.createUser(user);
    }

    @DeleteMapping(USERS_PATH)
    public void deleteAllUsers() {
        userService.deleteAllUsers();
    }
}
