package com.example.interview.controller.dto;

import lombok.Data;

@Data
public class UserDto {

    private Long userId;

    private String userGuid;

    private String userName;
}
