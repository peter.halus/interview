package com.example.interview.service;

import com.example.interview.controller.dto.UserDto;
import com.example.interview.mapping.UserMapper;
import com.example.interview.model.User;
import com.example.interview.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserService(UserRepository userRepository) {
        this.userRepository = Objects.requireNonNull(userRepository,"userRepository must not be null");
    }


    public List<UserDto> getAllUsers() {
        List<User> users = userRepository.findAll();
        List<UserDto> userDtos = users.stream().map(a -> UserMapper.INSTANCE.userToUserDto(a))
                .collect(Collectors.toList());

        return userDtos;
    }

    public void createUser(UserDto userRequest) {
        User user = UserMapper.INSTANCE.userDtoToUser(userRequest);
        userRepository.save(user);
    }

    public void deleteAllUsers() {
        userRepository.deleteAll();
    }
}
