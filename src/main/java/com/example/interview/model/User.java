package com.example.interview.model;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="SUSERS")
@Data
public class User {

    @Id
    @Column
    private Long userId;
    @Column
    private String userGuid;
    @Column
    private String userName;
}
