package com.example.interview.jms;

import com.example.interview.controller.dto.UserDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.*;

import javax.jms.Queue;

@RestController
@RequestMapping(
        path = "/api/produce",
        produces = "application/json"
)
public class Producer {

    private static final Logger logger = LoggerFactory.getLogger(Producer.class);

    @Autowired
    private JmsTemplate jmsTemplate;

    @Autowired
    private Queue queue;

    @PostMapping("/message")
    public UserDto sendMessage(
            @RequestHeader("Action") String action,
            @RequestBody(required=false) UserDto userDto) {

        logger.info("Producer Message action - " + action);
        ActionType at = ActionType.fromString(action);

        if(ActionType.INSERT.equals(at)){
            logger.info(userDto.toString());
        }

        try {
            ObjectMapper mapper = new ObjectMapper();
            String studentAsJson = mapper.writeValueAsString(userDto);

            jmsTemplate.convertAndSend(queue, studentAsJson, message ->  {
                message.setJMSType(at.name());
//                message.setStringProperty("action", "Vloz");
                return message;
            } );
        } catch (Exception e) {
            e.printStackTrace();
        }
        return userDto;
    }
}
