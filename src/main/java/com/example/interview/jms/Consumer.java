package com.example.interview.jms;

import com.example.interview.controller.dto.UserDto;
import com.example.interview.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

import javax.jms.JMSException;
import java.util.List;
import java.util.Objects;


@Component
public class Consumer {

    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);

    private final UserService userService;

    @Autowired
    public Consumer(UserService userService) {
        this.userService = Objects.requireNonNull(userService,"userService must not be null");
    }


    @JmsListener(destination = "queue-name", selector = "JMSType = 'INSERT' OR JMSType = 'SELECT' OR JMSType = 'DELETE_ALL'")
    public void consumeMessage(Object message) {
        logger.info("Consumer Message type - " + ((ActiveMQTextMessage) message).getType());
        String s = ((ActiveMQTextMessage) message).getType();

        if(ActionType.INSERT.name().equals(s)) {
            try {
                String jsonString = ((ActiveMQTextMessage) message).getText();
                UserDto userDto = new ObjectMapper().readValue(jsonString, UserDto.class);
                logger.info(userDto.toString());
                userService.createUser(userDto);
            } catch (JMSException e) {
                e.printStackTrace();
            } catch (JsonMappingException e) {
                e.printStackTrace();
            } catch (JsonProcessingException e) {
                e.printStackTrace();
            }
        }
        else if(ActionType.SELECT.name().equals(s)) {
            List<UserDto> userDtoList = userService.getAllUsers();
            if(!userDtoList.isEmpty()) {
                logger.info(userDtoList.toString());
            }
            else {
                logger.info("Databaza je prazdna!");
            }

        }
        else if(ActionType.DELETE_ALL.name().equals(s)) {
            userService.deleteAllUsers();
        }
        else {
            logger.info("nic");
        }
    }
}
