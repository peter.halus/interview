package com.example.interview.jms;

public enum ActionType {

    INSERT("Vloz"),
    SELECT("Vypis"),
    DELETE_ALL("ZmazVsetko");

    private final String value;


    ActionType(String value) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    public static ActionType fromString(String value) {
        for (ActionType actionType : ActionType.values()) {
            if (actionType.value.equals(value)) {
                return actionType;
            }
        }
        throw new IllegalArgumentException("ActionType - No constant with value " + value + " found");
    }
}
