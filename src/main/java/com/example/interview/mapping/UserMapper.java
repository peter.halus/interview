package com.example.interview.mapping;

import com.example.interview.controller.dto.UserDto;
import com.example.interview.model.User;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper( UserMapper.class );

    @Mappings({
            @Mapping(source = "userRequest.userId", target = "userId"),
            @Mapping(source = "userRequest.userGuid", target = "userGuid"),
            @Mapping(source = "userRequest.userName", target = "userName")
    })
    public User userDtoToUser(UserDto userRequest);

    @Mappings({
            @Mapping(source = "user.userId", target = "userId"),
            @Mapping(source = "user.userGuid", target = "userGuid"),
            @Mapping(source = "user.userName", target = "userName")
    })
    public UserDto userToUserDto(User user);
}
